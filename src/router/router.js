import { createRouter, createWebHistory } from "vue-router";

import Home from "../views/Home.vue";
import ProductPage from "../views/ProductPage.vue";
import WishList from "../views/WishList.vue";

const routes = [
    {
        path: "/",
        component: Home,
        name: "Home",
    },
    {
        path: "/product/:id",
        component: ProductPage,
        name: "ProductPage",
    },
    {
        path: "/wish-list",
        component: WishList,
        name: "WishList",
    },
    // { path: "/shopping-cart", component: ShoppingCart, name: "ShoppingCart" },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;
